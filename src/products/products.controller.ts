import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private productsService:ProductsService) {
  }


  @Post()
  createProduct(@Body() dto:CreateProductDto){
    return this.productsService.create(dto)
  }
}
