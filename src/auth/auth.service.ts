import { Body, HttpException, HttpStatus, Injectable, Post, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs'

@Injectable()
export class AuthService {

  constructor(private userService:UsersService,
              private jwtService:JwtService) {
  }

  async login(userDto:CreateUserDto){
    const user2= await this.validateUser(userDto)
    return  this.generateToken(user2)

  }
  async registration(userDto:CreateUserDto){
    const candidate= await this.userService.getUsersByEmail(userDto.email)
    console.log(candidate)
    if (candidate){
      throw new HttpException('Пользователь с таким Эмеилом уже существует',HttpStatus.BAD_REQUEST)
    }
    const hashPassword= await bcrypt.hash(userDto.password,5)
    const user= await this.userService.createUser({...userDto,password:hashPassword})
    return user
  }
  private async generateToken(user2:any){
    const payload={user2}
    return{
      token: this.jwtService.sign(payload)
    }
  }

  private async validateUser(userDto: CreateUserDto) {
    const user= await this.userService.getUsersByEmail(userDto.email)
    const passwordEquals= await bcrypt.compare(userDto.password,user.password)
    if ( user && passwordEquals){
      return user
    }
    throw new UnauthorizedException({message:'Некоректный емеил или пароль'})

    
  }
}




