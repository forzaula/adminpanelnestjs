import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from '../schemas/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { RolesService } from '../roles/roles.service';
import { User_roles, User_rolesDocument } from '../schemas/user_roles.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>,
              @InjectModel(User_roles.name) private user_rolesModel:Model<User_rolesDocument>,

  private roleService:RolesService) {
  }

  async createUser(dto:CreateUserDto){
    const user= await this.userModel.create(dto);
    const role= await this.roleService.getRoleById("6194c2be72e4cd9e92a7fa06")
    const userRole = await this.user_rolesModel.create({user_id: user._id, role_id: role})
    await user.$set('roles',role)

    const user2=[]
    user2.push(userRole,user)
    return user2




  }

  async getAllUsers(){
    const users= await this.userModel.find()

    return users


  }
  async getUsersByEmail(email:string){
    const user=await this.userModel.findOne({email:email})
    return user
  }
}


