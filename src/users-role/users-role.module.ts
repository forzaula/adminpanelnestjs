import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User_roles, User_rolesSchema } from '../schemas/user_roles.schema';
import { UsersRoleService } from './users-role.service';

@Module({
  imports:[MongooseModule.forFeature([
    {name:User_roles.name,schema:User_rolesSchema}
  ]),],
  providers: [UsersRoleService],

  exports:[MongooseModule],
})
export class UsersRoleModule {}
