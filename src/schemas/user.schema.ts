import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


export type UserDocument = User & Document;


@Schema()
export class User{
  @Prop()
  first_name:string

  @Prop()
  last_name:string

  @Prop({ required: true,allowNull: false})
  email:string

  @Prop({ required: true })
  password:string

  @Prop({default:false })
  banned:boolean

  @Prop()
  banReason:string






}

export const UserSchema=SchemaFactory.createForClass(User)