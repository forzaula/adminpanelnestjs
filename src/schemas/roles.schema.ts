import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


export type RolesDocument = Roles & Document;



@Schema()
export class Roles{

 @Prop({type:String,unique:true,default:"USER"})
  value:string




}

export const RolesSchema=SchemaFactory.createForClass(Roles)