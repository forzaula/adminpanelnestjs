import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Roles, RolesDocument } from 'src/schemas/roles.schema';
import { Model } from 'mongoose';

@Injectable()
export class RolesService {

  constructor(@InjectModel(Roles.name) private rolesModel:Model<RolesDocument> ) {
  }

  async createRole(dto: CreateRoleDto){
    const role=await this.rolesModel.create(dto)
    return role

  }
  async getAllRoles(): Promise<Roles[]>{
    const role=await this.rolesModel.find().exec()
    return role
  }
  async getRoleById(id:string):Promise<Roles>{
    const role=await this.rolesModel.findById(id)
    return role
  }
}
