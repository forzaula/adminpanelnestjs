import { forwardRef, Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../schemas/user.schema';
import { RolesModule } from '../roles/roles.module';
import { UsersRoleModule } from '../users-role/users-role.module';
import { AuthModule } from '../auth/auth.module';
import { Roles } from '../schemas/roles.schema';
import {ProductsModule} from "../products/products.module";

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports:[
    MongooseModule.forFeature([
      {name:User.name,schema:UserSchema}
    ]),
    RolesModule,
    AuthModule,
    UsersRoleModule,
    ProductsModule,
    forwardRef(()=>AuthModule),
  ],
  exports:[UsersService]
})
export class UsersModule {}
