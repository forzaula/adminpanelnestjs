import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Roles } from './roles.schema';
import { User } from './user.schema';
import { Document } from 'mongoose';

export type User_rolesDocument = User_roles & Document;



@Schema()
export class User_roles {

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user_id: User;
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Roles' })
  role_id: Roles;

}

export const User_rolesSchema=SchemaFactory.createForClass(User_roles)