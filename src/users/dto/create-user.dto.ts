import {IsEmail, IsString, Length } from "class-validator"




export class CreateUserDto{
  @IsString({message:'have to be string'})
  @IsEmail({message:'Should be Email'})
  readonly email:string
  @IsString({message:'have to be string'})
  @Length(4,16,{message:'no less than 4 and more than 16'})
  readonly password:string
  readonly role:string

}