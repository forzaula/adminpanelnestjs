import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from './user.schema';


export type ProductDocument = Product & Document;


@Schema()
export class Product{
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop({ required: true })
  title:string

  @Prop({ required: true })
  price:number

}

export const ProductSchema=SchemaFactory.createForClass(Product)