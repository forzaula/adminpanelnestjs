import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Product, ProductDocument } from '../schemas/product.schema';
import { Model } from 'mongoose';

@Injectable()
export class ProductsService {

  constructor(@InjectModel(Product.name) private productsModel:Model<ProductDocument> ) {
  }

  async create(dto:CreateProductDto){
    const product= await this.productsModel.create(dto)
    return product

  }
}
