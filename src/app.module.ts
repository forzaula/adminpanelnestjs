import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { UsersRoleModule } from './users-role/users-role.module';
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './products/products.module';


@Module({
  imports:[
    MongooseModule.forRoot('mongodb+srv://admin:admin@cluster0.xshyl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'),
    UsersModule,
    RolesModule,
    UsersRoleModule,
    AuthModule,
    ProductsModule],
  controllers:[],
  providers: [],
})
export class AppModule{}