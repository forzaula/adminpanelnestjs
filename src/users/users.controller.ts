import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import {ValidationPipe} from "../pipes/validation.pipe";
import { UsePipes } from '@nestjs/common';

@Controller('users')
export class UsersController {
  constructor(private usersService:UsersService) {}


  @Post()
  @UsePipes(ValidationPipe)
  create(@Body() userDto:CreateUserDto){
    return this.usersService.createUser(userDto)
  }
  @UseGuards(JwtAuthGuard)
  @Get()
  getAll(){
    return this.usersService.getAllUsers()

  }
}
